# Serverless Blog on Azure

A serverless blog, this repository contains the frontend for the blog. Basically, I was working on a project with Packt, when I explored the beauty of Azure Storage and Azure Functions, so I wanted to do a project that mixes the best from both worlds and provides something. There are several things that I do not like, but I need to do that, in order to keep the project simple and less painful&mdash;for myself. 

The overall concept of serverless is that nothing in this overall blog is asking me to manage anything from operational standpoint. I am writing serverless tasks, that provide me with the information (more on this coming in other documents). Users can request the content, and the application will provide the content without having to manage anything server-oriented. The server-side tasks are accomplished by Azure Functions, and utilizing a cloud-native architecture, I can make the best possible use of bindings in Azure Functions, to provide the response to the users. 

Ourall (I know it is overall&dash;but I typed it that way, so I am keeping it that way) tech stack for this project includes,   
1. Azure Functions
2. Azure Storage
3. Azure Application Insights

Most of these elements are the important ones, such as the Azure Storage and Azure Functions, but some are less important and might only add some value to understand how users are interacting with your applications. 

This project would be open sourced, and you can try it out yourself. Just connect your own services, and products, and happy blogging. :smile: 
